﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class MoveTowardsObjects : Action
{
    // The speed of the object
    public SharedFloat speed;
    // The transform that the object is moving towards
    public SharedGameObject targetObj;

    public GameObject firstObj;

    public float distanceChecked;
    public override void OnStart()
    {
        speed = speed.Value;
    }

    public override TaskStatus OnUpdate()
    {
        targetObj.Value = (GameObject)targetObj.GetValue();

        float dist = Vector3.Distance(firstObj.transform.position, targetObj.Value.transform.position);
        if (dist < distanceChecked)
        {
            return TaskStatus.Failure;
        }
        // We haven't reached the target yet so keep moving towards it
        firstObj.transform.position = Vector3.MoveTowards(firstObj.transform.position, targetObj.Value.transform.position, (speed.Value * Time.deltaTime));
        return TaskStatus.Running;
    }
}