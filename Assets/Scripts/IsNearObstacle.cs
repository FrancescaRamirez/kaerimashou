﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class IsNearObstacle : Conditional
{
    public SharedGameObject obstacle;
    public GameObject[] obstacles;
    public SharedFloat speed;
    //public float distance;
    public bool found;

    //GameObject o;

    public GameObject currentObj;
    // Called once when the node is created
    public override void OnAwake()
    {

    }

    // Called when the owner (BehaviourTree or ActionState) is enabled
    public override void OnStart()
    {
        obstacles = GameObject.FindGameObjectsWithTag("Obstacle");
        //obstacle = obstacle.Value;
        
        speed = 5;
        found = false;
        

        Debug.Log("Start");
        //obstacle.Value = FindNearestWall();
    }

    // This function is called when the node is in execution
    public override TaskStatus OnUpdate()
    {
        obstacle.Value = FindNearestWall();
        if(obstacle.Value != null)
        {
            float dist = Vector3.Distance(currentObj.transform.position, obstacle.Value.transform.position);
            if (dist < 0.6)
            {
                return TaskStatus.Success;
            }
            else
            {
                return TaskStatus.Running;
            }
            
        }
        else
        {
            return TaskStatus.Failure;
        }

        // Never forget to set the node status
        //return Status.Running;
    }

    public GameObject FindNearestWall()
    {
        float closestDistance = Mathf.Infinity;
        GameObject closestObstacle = null;

        foreach (GameObject o in obstacles)
        {
            float dist = Vector3.Distance(currentObj.transform.position, o.transform.position);
            if (dist < closestDistance)
            {
                closestDistance = dist;
                closestObstacle = o;
            }
        }
        return closestObstacle;
    }
}