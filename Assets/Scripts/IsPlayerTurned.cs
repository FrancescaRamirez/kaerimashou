﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class IsPlayerTurned : Conditional
{
    public SharedGameObject playerObject;
    public CharacterControl playerController;
    public bool isPlayerTurned;

    // Called once when the node is created
    public override void OnAwake() {
        
    }

    // Called when the node starts its execution
    public override void OnStart() {
        playerObject = playerObject.Value;
        playerController = playerObject.Value.GetComponent<CharacterControl>();
    }

    // This function is called when the node is in execution
    public override TaskStatus OnUpdate()
    {
        isPlayerTurned = playerController.isTurned;

        // Do stuff
        if (!isPlayerTurned)
        {
            Debug.Log("Player is not turned around");
            return TaskStatus.Success;
        }
        else
        {
            Debug.Log("Player is turned around");
            return TaskStatus.Failure;
        }
        // Never forget to set the node status
        //return TaskStatus.Running;
    }
}