﻿using UnityEngine;

[ExecuteInEditMode]
public class Sensor : MonoBehaviour
{
    public float magnitude;
    public Color safeColor;
    public Color warningColor;
    public Color hitColor;
    public bool hit;
    public float hitDistance;
    public float hitPercentage;
    public float sensitivity;

    void Update()
    {
        //for magnitude, you can change the num depending on how fast the car is going so it has more time to react
        //for other hits
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hitInfo;
        LayerMask layerMask = LayerMask.GetMask("Obstacle");
        hit = Physics.Raycast(ray, out hitInfo, magnitude, layerMask);

        if (hit)
        {
            hitDistance = hitInfo.distance;
            hitPercentage = 1 - (hitDistance / magnitude);
            //if it's 0 it isn't close, if 1 then it's close
            Debug.DrawLine(ray.origin, hitInfo.point, Color.Lerp(warningColor, hitColor, hitPercentage));
        }
        else
        {
            Debug.DrawLine(transform.position, transform.position + transform.forward * magnitude, safeColor);
        }

    }
}
