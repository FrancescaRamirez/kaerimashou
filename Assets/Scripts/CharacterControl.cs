﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CharacterControl : MonoBehaviour
{
    public CharacterController controller;
    public Transform playerBody;
    public float playerSpeed;
    public bool isTurned;
    public bool isStopped;
    public float yRot;
    public Vector3 moveSide;

    public float stamina;
    public float maxStamina;

    public Slider slider;

    public Animator animator;

    public bool isCaught;
    public bool isHome;

    public AudioSource audioSource;

    public SwitchScenes switchScenes;
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        playerBody = GetComponent<Transform>();
        playerSpeed = 2.0f;
        isTurned = false;
        isStopped = false;
        isCaught = false;
        isHome = false;

        stamina = 3f;
        maxStamina = 3f;


        animator = GetComponentInChildren<Animator>();

        slider.maxValue = maxStamina;

        switchScenes = GetComponent<SwitchScenes>();
    }

    // Update is called once per frame
    void Update()
    {
        slider.value = stamina;

        float yPos = playerBody.position.y;
        yPos = Mathf.Clamp(yPos, 0, 0);
        playerBody.position.y.Equals(yPos);

        yRot = playerBody.localRotation.eulerAngles.y;
        yRot = Mathf.Clamp(yRot, 5, 180);

        playerBody.localRotation = Quaternion.Euler(0f, yRot, 0f);

        //player can only move forward
        if (isTurned == false && isStopped == false)
        {
            
            Vector3 moveUp = new Vector3(0, 0, 1);
            controller.Move(moveUp * Time.deltaTime * playerSpeed);
            //player can move left or right

            animator.speed = 1;

            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            {
                if (stamina > 0)
                {
                    stamina -= 1f * Time.deltaTime;
                    playerSpeed = 6.5f;

                    animator.speed = 3;
                }
                else
                {
                    stamina = 0;
                    playerSpeed = 2f;
                }
            }
            else
            {
                playerSpeed = 2f;
            }
        }

        if (stamina == maxStamina)
        {
            slider.gameObject.SetActive(false);
            audioSource.volume = 0.1f;
        }
        else
        {
            slider.gameObject.SetActive(true);
            audioSource.volume = 0.5f;
        }

        //player can move left to right to dodge walls/look around
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow))
        {
            if (yRot < 90)
            {
                moveSide = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
            }
            else
            {
                moveSide = new Vector3(-Input.GetAxis("Horizontal"), 0, 0);
            }
            
            controller.Move(moveSide * Time.deltaTime * 3);
        }

        //player can turn around 180 and back to check for the yandere
        Mathf.Clamp(playerBody.rotation.eulerAngles.y, 0, 180);
        if(Input.GetKey(KeyCode.E))
        {
            //hold down e to look behind
            isTurned = true;
            isStopped = true;
            Vector3 rotate = new Vector3(0, 5, 0);
            playerBody.Rotate(rotate);

            
        }
        if (yRot > 100)
        {
            stamina += Time.deltaTime;
            if (stamina > maxStamina)
            {
                stamina = maxStamina;
            }
        }
        if (Input.GetKey(KeyCode.Q))
        {
            //hold down to turn back
            isTurned = true;
            isStopped = true;
            Vector3 rotate = new Vector3(0, -5, 0);
            playerBody.Rotate(rotate);
        }
        if(Input.GetKeyUp(KeyCode.Q) && yRot == 5)
        {
            //player will only start moving again if they turn fully around
            isTurned = false;
            isStopped = false;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Stalker")
        {
            slider.gameObject.SetActive(false);
            audioSource.volume = 0.2f;
            playerSpeed = 0;
            isCaught = true;
            isStopped = true;
            Vector3 rotate = new Vector3(0, -180, 0);
            playerBody.Rotate(rotate);
            yRot = Mathf.Clamp(yRot, -180, -180);

            switchScenes.Invoke("LoseLoad", 3);
        }
        else if(other.tag == "Home")
        {
            slider.gameObject.SetActive(false);
            audioSource.volume = 0.2f;
            playerSpeed = 0;
            isCaught = false;
            isStopped = true;
            isHome = true;

            switchScenes.Invoke("Win2Load", 1);
        }
    }

    public void caughtEnemy()
    {
        slider.gameObject.SetActive(false);
        audioSource.volume = 0.2f;
        playerSpeed = 0;
        isCaught = false;
        isStopped = true;
        float i = 180 - playerBody.rotation.eulerAngles.y;
        Vector3 rotate = new Vector3(0, i, 0);
        playerBody.Rotate(rotate);

        switchScenes.Invoke("Win1Load", 4);
    }
}
