﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchScenes : MonoBehaviour
{

    public Scene scene;
    public string sceneName;

    // Start is called before the first frame update
    void Start()
    {
        scene = SceneManager.GetActiveScene();
        sceneName = scene.name;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MenuLoad()
    {
        SceneManager.LoadScene(0);
    }
    public void StoryLoad()
    {
        SceneManager.LoadScene(1);
    }

    public void GameLoad()
    {
        SceneManager.LoadScene(2);
    }

    public void Win1Load()
    {
        SceneManager.LoadScene(3);
    }

    public void Win2Load()
    {
        SceneManager.LoadScene(4);
    }

    public void LoseLoad()
    {
        SceneManager.LoadScene(5);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
