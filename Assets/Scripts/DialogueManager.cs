﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    public Queue<string> sentences;

    public TextMeshProUGUI nameText;
    public TextMeshProUGUI dialogueText;

    public SwitchScenes sceneSwitch;

    public GameObject playerChar;
    public GameObject enemyChar;
    public Animator playerAnim;
    public Animator enemyAnim;

    public string playerName = "Player:";
    public string enemyName = "Enemy:";
    public string playerSprite = "playerSprite:";
    public string enemySprite = "enemySprite:";

    public string noName = "None:";

    public AudioSource bgm;
    public AudioSource bgmScary;

    public Animator winAni;

    // Start is called before the first frame update
    void Awake()
    {
        bgm.Play();
        sentences = new Queue<string>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartDialogue(Dialogue dialogue)
    {
        Debug.Log("Starting dialogue");

        sentences.Clear();

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = sentences.Dequeue();

        //adjust according to name
        if (sentence.Contains(playerName))
        {
            nameText.text = "Yu";

            sentence = sentence.Replace(playerName, "");

            if (sentence.Contains(playerSprite))
            {
                sentence = ApplySprite(sentence);
            }

            if (sentence.Contains(enemySprite))
            {
                sentence = ApplySprite(sentence);
            }

            

        }
        if (sentence.Contains(enemyName))
        {
            nameText.text = "Miyu";

            sentence = sentence.Replace(enemyName, "");

            if (sentence.Contains(playerSprite))
            {
                sentence = ApplySprite(sentence);
            }

            if (sentence.Contains(enemySprite))
            {
                sentence = ApplySprite(sentence);
            }

            if (sentence.Contains("BGMSTOP"))
            {
                bgm.Stop();
                sentence = sentence.Replace("BGMSTOP", "");
            }

            if (sentence.Contains("BGMPLAY"))
            {
                bgmScary.Play();
                sentence = sentence.Replace("BGMPLAY", "");
            }
        }
        if (sentence.Contains(noName))
        {
            nameText.text = "";

            sentence = sentence.Replace(noName, "");

            if (sentence.Contains(playerSprite))
            {
                sentence = ApplySprite(sentence);
            }

            if (sentence.Contains(enemySprite))
            {
                sentence = ApplySprite(sentence);
            }
        }

        //dialogueText.text = sentence;

        StopAllCoroutines();

        StartCoroutine(TypeSentence(sentence));
    }

    IEnumerator TypeSentence (string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    void EndDialogue()
    {
        Debug.Log("End of Conversation");

        //make if/else checking for which scene is currently open and load accordingly

        if (sceneSwitch.sceneName == "StartingStory")
        {
            sceneSwitch.Invoke("GameLoad", 0);
        }
        else if (sceneSwitch.sceneName == "Lose")
        {
            dialogueText.text = "L E T ' S G O H O M E T O G E T H E R";
            playerChar.SetActive(false);
            enemyAnim.SetBool("isCloseUp", true);
            sceneSwitch.Invoke("MenuLoad", 1);
        }
        else if(sceneSwitch.sceneName == "Win1")
        {
            sceneSwitch.Invoke("MenuLoad", 1);
        }
        else if(sceneSwitch.sceneName == "Win2")
        {
            nameText.text = "Miyu";
            dialogueText.text = "'All to o u r s e l v e s'";
            winAni.SetTrigger("fadeOut");
            sceneSwitch.Invoke("MenuLoad", 3);
        }

    }

    string ApplySprite(string sen)
    {

        if (sen.Contains(playerSprite))
        {
            //set active sprite
            if (sen.Contains(playerSprite + "active"))
            {
                playerChar.SetActive(true);
                sen = sen.Replace(playerSprite + "active", "");
            }
            if (sen.Contains(playerSprite + "inactive"))
            {
                playerChar.SetActive(false);
                sen = sen.Replace(playerSprite + "inactive", "");
            }

            //change animations
            if (sen.Contains(playerSprite + "idle"))
            {
                playerAnim.SetBool("isScared", false);
                playerAnim.SetBool("isScared", false);

                playerAnim.SetBool("isIdle", true);
                sen = sen.Replace(playerSprite + "idle", "");
            }
            if (sen.Contains(playerSprite + "scared"))
            {
                playerAnim.SetBool("isIdle", false);
                playerAnim.SetBool("isWalkOut", false);

                playerAnim.SetBool("isScared", true);
                sen = sen.Replace(playerSprite + "scared", "");
            }
            if (sen.Contains(playerSprite + "walkOut"))
            {
                playerAnim.SetBool("isIdle", false);
                playerAnim.SetBool("isScared", false);

                playerAnim.SetBool("isWalkOut", true);
                sen = sen.Replace(playerSprite + "walkOut", "");
            }
        }

        if (sen.Contains(enemySprite))
        {
            //set active sprite
            if (sen.Contains(enemySprite + "active"))
            {
                enemyChar.SetActive(true);
                sen = sen.Replace(enemySprite + "active", "");
            }
            if (sen.Contains(enemySprite + "inactive"))
            {
                enemyChar.SetActive(false);
                sen = sen.Replace(enemySprite + "inactive", "");
            }

            //change animations
            if (sen.Contains(enemySprite + "idle"))
            {
                enemyAnim.SetBool("isCrazy", false);
                enemyAnim.SetBool("isAngry", false);
                enemyAnim.SetBool("isYan", false);
                enemyAnim.SetBool("isDrop", false);
                enemyAnim.SetBool("isWalkIn", false);

                enemyAnim.SetBool("isIdle", true);
                sen = sen.Replace(enemySprite + "idle", "");
            }
            if (sen.Contains(enemySprite + "crazy"))
            {
                enemyAnim.SetBool("isIdle", false);
                enemyAnim.SetBool("isAngry", false);
                enemyAnim.SetBool("isYan", false);
                enemyAnim.SetBool("isDrop", false);
                enemyAnim.SetBool("isWalkIn", false);

                enemyAnim.SetBool("isCrazy", true);
                sen = sen.Replace(enemySprite + "crazy", "");
            }
            if (sen.Contains(enemySprite + "angry"))
            {
                enemyAnim.SetBool("isIdle", false);
                enemyAnim.SetBool("isCrazy", false);
                enemyAnim.SetBool("isYan", false);
                enemyAnim.SetBool("isDrop", false);
                enemyAnim.SetBool("isWalkIn", false);

                enemyAnim.SetBool("isAngry", true);
                sen = sen.Replace(enemySprite + "angry", "");
            }
            if (sen.Contains(enemySprite + "yan"))
            {
                enemyAnim.SetBool("isIdle", false);
                enemyAnim.SetBool("isCrazy", false);
                enemyAnim.SetBool("isAngry", false);
                enemyAnim.SetBool("isDrop", false);
                enemyAnim.SetBool("isWalkIn", false);

                enemyAnim.SetBool("isYan", true);
                sen = sen.Replace(enemySprite + "yan", "");
            }
            if (sen.Contains(enemySprite + "drop"))
            {
                enemyAnim.SetBool("isIdle", false);
                enemyAnim.SetBool("isCrazy", false);
                enemyAnim.SetBool("isAngry", false);
                enemyAnim.SetBool("isYan", false);
                enemyAnim.SetBool("isWalkIn", false);

                enemyAnim.SetBool("isDrop", true);
                sen = sen.Replace(enemySprite + "drop", "");
            }
            if (sen.Contains(enemySprite + "walkIn"))
            {
                enemyAnim.SetBool("isIdle", false);
                enemyAnim.SetBool("isCrazy", false);
                enemyAnim.SetBool("isAngry", false);
                enemyAnim.SetBool("isYan", false);
                enemyAnim.SetBool("isDrop", false);

                enemyAnim.SetBool("isWalkIn", true);
                sen = sen.Replace(enemySprite + "walkIn", "");
            }
            
        }

        return sen;
    }
}
