﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class SpeedChange : Action
{
    public SharedFloat speed;
    public float newSpeed;
    public float timer;

    // Called once when the node is created
    public override void OnAwake()
    {
        
    }

    // Called when the node starts its execution
    public override void OnStart()
    {
        timer = 0;
    }

    // This function is called when the node is in execution
    public override TaskStatus OnUpdate()
    {
        timer -= 1 * Time.deltaTime;

        if (timer <= 0)
        {
            newSpeed = (int) Random.Range(3.0f, 6f);
            speed.Value = newSpeed;

            timer = Random.Range(1, 3);

            return TaskStatus.Success;
        }

        return TaskStatus.Running;
    }
}
