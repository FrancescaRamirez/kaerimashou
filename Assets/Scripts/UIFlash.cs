﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFlash : MonoBehaviour
{

    public Animator imageAnim;
    public GameObject player;
    public GameObject enemy;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float dist = Vector3.Distance(player.transform.position, enemy.transform.position);
        if (dist <= 15)
        {
            imageAnim.SetBool("isFlash", true);
            imageAnim.SetBool("Idle", false);
            imageAnim.speed = 1;

            if (dist <= 10)
            {
                imageAnim.SetBool("Idle", false);
                imageAnim.speed = 3;
            }
        }
        else
        {
            imageAnim.SetBool("Idle", true);
        }
    }
}
