﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour
{
    public Camera mainCam;

    public bool useStaticBill;
    // Start is called before the first frame update
    void Start()
    {
        mainCam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if(!useStaticBill)
        {
            transform.LookAt(mainCam.transform);
        }
        else
        {
            transform.rotation = mainCam.transform.rotation;
        }
        

        transform.rotation = Quaternion.Euler(0f, transform.rotation.eulerAngles.y, 0f);
    }
}
