﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScreen : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Resume()
    {
        AudioListener.pause = false;
        Time.timeScale = 1f;
    }

    public void Pause()
    {
        AudioListener.pause = true;
        Time.timeScale = 0f;
    }
}
