﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class IsPlayerHome : Conditional
{
    public SharedGameObject myBlackboard;
    public SharedGameObject playerObj;
    public CharacterControl characterController;
    public bool isHome;
    public int i;

    public override void OnStart()
    {
        playerObj = playerObj.Value;
        characterController = playerObj.Value.GetComponentInChildren<CharacterControl>();
        isHome = characterController.isHome;
    }

    public override TaskStatus OnUpdate()
    {
        isHome = characterController.isHome;
        
        if (isHome == false)
        {
            // Update status
            return TaskStatus.Success;
        }
        else
        {
            // Update status
            return TaskStatus.Failure;
        }
    }
}
