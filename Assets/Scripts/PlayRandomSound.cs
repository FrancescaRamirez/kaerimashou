﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayRandomSound : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip[] audioClips;

    public float timer;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        timer = Random.Range(5, 10);
    }

    // Update is called once per frame
    void Update()
    {
        if (timer > 0)
        {
            timer -= 1 * Time.deltaTime;
        }
        else
        {
            audioSource.clip = audioClips[Random.Range(0, audioClips.Length)];
            audioSource.PlayOneShot(audioSource.clip);
            timer = Random.Range(5, 10);
        }
    }
}
