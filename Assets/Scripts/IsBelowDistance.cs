﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class IsBelowDistance : Conditional
{
    public GameObject currentGameObject;
    public SharedGameObject targetGameObject;

    public float distanceChecked;

    public override TaskStatus OnUpdate()
    {
        float dist = Vector3.Distance(currentGameObject.transform.position, targetGameObject.Value.transform.position);
        if (dist < distanceChecked)
        {
            return TaskStatus.Success;
        }
        else
        {
            return TaskStatus.Failure;
        }
    }
}
