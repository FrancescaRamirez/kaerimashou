﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class DodgeObstacles : Action
{
    public SharedGameObject surrenderTransform;

    public Sensor frontRightSensor;
    public Sensor frontLeftSensor;
    public Sensor frontRightAngledSensor;
    public Sensor frontLeftAngledSensor;

    int multiplier;

    public SharedGameObject obstacle;

    // Called once when the node is created
    public override void OnAwake() { }


    // Called when the node starts its execution
    public override void OnStart() { }
    public override TaskStatus OnUpdate()
    {
        if(frontRightAngledSensor.hit || frontLeftAngledSensor.hit || frontRightSensor.hit || frontLeftSensor.hit)
        {
            if(obstacle.Value != null)
            {
                obstacle.Value = null;
            }
        }
        

        Debug.DrawLine(transform.position, transform.position + surrenderTransform.Value.transform.position, Color.cyan);

        // steer away slightly from obstacles that are to the side
        if (frontRightAngledSensor.hit)
        {
            multiplier = 1;
            //add to avoid
            transform.Translate(multiplier * 1, 0 , 0);
        }
        if (frontLeftAngledSensor.hit)
        {
            multiplier = -1;
            //subtract to avoid
            transform.Translate(multiplier * 1, 0, 0);
        }

        // steer away from obstacles that are to the side
        if (frontRightSensor.hit)
        {//add to avoid
            multiplier = 1;
            transform.Translate(multiplier * 2, 0, 0);
        }
        if (frontLeftSensor.hit)
        {//subtract to avoid
            multiplier = -1;
            transform.Translate(multiplier * 2, 0, 0);
        }

        transform.position = Vector3.MoveTowards(transform.position, surrenderTransform.Value.transform.position, 10 * Time.deltaTime);

        return TaskStatus.Success;
    }
}
