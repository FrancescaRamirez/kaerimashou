﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class IsSeen : Conditional
{
    public SharedGameObject player;
    public Vision playerVision;
    public bool isSeen;
    public int i;

    public override void OnStart()
    {
        player = player.Value;
        playerVision = player.Value.GetComponentInChildren<Vision>();
        isSeen = playerVision.isSeen;
    }

    public override TaskStatus OnUpdate()
    {
        isSeen = playerVision.isSeen;
        if (isSeen == false)
        {
            // Update status
            return TaskStatus.Success;
        }
        else
        {
            // Update status
            return TaskStatus.Failure;
        }
    }
}