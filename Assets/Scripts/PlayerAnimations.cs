﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour
{
    public CharacterControl characterControl;
    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        characterControl = GetComponent<CharacterControl>();
        animator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //player can only move forward
        if (characterControl.isTurned == false && characterControl.isStopped == false)
        {
            animator.SetBool("isWalking", true);
            animator.SetBool("isTurnLeft", false);
            animator.SetBool("isTurnRight", false);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            //hold down e to look behind
            animator.SetBool("isWalking", false);
            animator.SetBool("isTurnRight", true);
            animator.SetBool("isTurnLeft", false);
        }

        if (Input.GetKeyUp(KeyCode.E))
        {
            //hold down e to look behind
            animator.SetBool("isWalking", false);
            animator.SetBool("isTurnRight", false);
            animator.SetBool("isTurnLeft", false);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            //hold down to turn back
            animator.SetBool("isWalking", false);
            animator.SetBool("isTurnLeft", true);
            animator.SetBool("isTurnRight", false);
        }

        if (Input.GetKeyUp(KeyCode.Q))
        {
            //hold down e to look behind
            animator.SetBool("isWalking", false);
            animator.SetBool("isTurnRight", false);
            animator.SetBool("isTurnLeft", false);
        }
        else if (Input.GetKeyUp(KeyCode.Q) && characterControl.yRot == 2)
        {
            //player will only start moving again if they turn fully around
            animator.SetBool("isWalking", true);
            animator.SetBool("isTurnLeft", false);
            animator.SetBool("isTurnRight", false);
        }


        if (characterControl.isCaught == true)
        {
            animator.SetBool("isCaught", true);
        }
    }
}
