﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class OpenLinks : MonoBehaviour
{

    public void JunichiLink()
    {
        #if !UNITY_EDITOR
        openWindow("https://www.archdaily.com/910501/kyoto-xiaoman-koyori-plus-aterier-salt/5c55f440284dd15568000059-kyoto-xiaoman-koyori-plus-aterier-salt-photo");
        #endif
    }

    public void YoheiLink()
    {
        #if !UNITY_EDITOR
        openWindow("https://architectureau.com/articles/back-to-black-kyoto-terrace-house/?utm_source=ArchitectureAU&utm_campaign=b8bacd7340-AAU_2019_01_08&utm_medium=email&utm_term=0_e3604e2a4a-b8bacd7340-39739570#img-8");
        #endif
    }

    public void GooLink()
    {
        #if !UNITY_EDITOR
        openWindow("https://blog.goo.ne.jp/iriomoteriver/e/4b5e569e1ccae87ac971e8aaed0d236b");
        #endif
    }

    public void LibLink()
    {
        #if !UNITY_EDITOR
        openWindow("http://texturelib.com/texture/?path=/Textures/concrete/dirty/concrete_dirty_0003");
        #endif
    }

    public void PixLink()
    {
        #if !UNITY_EDITOR
        openWindow("https://seamless-pixels.blogspot.com/p/blog-page.html");
        #endif
    }

    public void PantsLink()
    {
        #if !UNITY_EDITOR
        openWindow("https://freesound.org/people/mypantsfelldown/sounds/465299/");
        #endif
    }

    public void MissLink()
    {
        #if !UNITY_EDITOR
        openWindow("https://freesound.org/people/missozzy/sounds/?page=4#sound");
        #endif
    }

    public void SlamLink()
    {
        #if !UNITY_EDITOR
        openWindow("https://freesound.org/people/slamaxu/sounds/509883/");
        #endif
    }

    public void VooLink()
    {
        #if !UNITY_EDITOR
        openWindow("https://search.creativecommons.org/photos/005e416e-aaed-4ce4-a7ef-f9b4c3ce61c0");
        #endif
    }

    public void BauLink()
    {
        #if !UNITY_EDITOR
        openWindow("https://search.creativecommons.org/photos/6ffdfce5-b98a-4151-922a-8d78797f3a8b");
        #endif
    }

    public void HotLink()
    {
        #if !UNITY_EDITOR
        openWindow("https://search.creativecommons.org/photos/67cbd2a3-8a32-49d1-abfe-1e5bad392c61");
        #endif
    }

    public void TonyLink()
    {
        #if !UNITY_EDITOR
        openWindow("https://www.vecteezy.com/vector-art/212440-oak-tree-silhouettes");
        #endif
    }

    public void TrashLink()
    {
        #if !UNITY_EDITOR
        openWindow("https://assetstore.unity.com/packages/3d/props/exterior/trash-can-23183");
        #endif
    }

    public void SkyLink()
    {
        #if !UNITY_EDITOR
        openWindow("https://assetstore.unity.com/packages/2d/textures-materials/sky/allsky-free-10-sky-skybox-set-146014");
        #endif
    }

    public void TernoxLink()
    {
        #if !UNITY_EDITOR
        openWindow("https://ternox.itch.io/vn-music");
        #endif
    }

    [DllImport("__Internal")]
    private static extern void openWindow(string url);


}
